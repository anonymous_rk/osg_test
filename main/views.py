from django.shortcuts import render, redirect
from .forms import RegsiterForm, LoginForm, OrderForm
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage




def home(request):
    return render(request, 'home.html')


def register(request):

    form = RegsiterForm()
    if request.method == 'POST':
        form = RegsiterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.role = 'Client'
            user.save()
            return redirect('login')
        else:
            print(form.errors)

    context = {'forms': form}
    return render(request, 'register.html', context)


def login_view(request):
    form = LoginForm()
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('home')

    context = {'form': form}
    return render(request, 'login.html', context)

@login_required()
def logout_view(request):
    logout(request)
    return redirect('login')


def book_order(request):
    form = OrderForm()
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.save()
            messages.success(request, 'You have succesfully ordered the book')
            return redirect('home')

    context = {'forms': form}

    return render(request, 'orders.html', context)