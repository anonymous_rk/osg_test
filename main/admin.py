from django.contrib import admin
from .models import CustomUser, Book, Order



admin.site.register(CustomUser)
admin.site.register(Book)
admin.site.register(Order)


