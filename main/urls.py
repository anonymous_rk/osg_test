from django.urls import path, include
from .views import register, login_view, home, logout_view, book_order


urlpatterns = [
    path('register/', register, name='register'),
    path('login/', login_view, name='login'),
    path('home/', home, name='home'),
    path('logout/', logout_view, name='logout'),
    path('order/', book_order, name="order")
]