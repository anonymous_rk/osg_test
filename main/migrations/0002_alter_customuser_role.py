# Generated by Django 3.2.5 on 2021-07-28 05:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='role',
            field=models.CharField(choices=[('Admin', 'Admin'), ('Salesman', 'Salesman'), ('Client', 'Client')], max_length=50, null=True),
        ),
    ]
