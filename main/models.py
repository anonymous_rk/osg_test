from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):

    USER_ROLES = (
        ('Admin', 'Admin'),
        ('Salesman', 'Salesman'),
        ('Client', 'Client'),
    )


    username = models.CharField(max_length=100, blank=True, null=True, unique=True)
    email = models.EmailField('email address', unique=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=13)
    role = models.CharField(max_length=50, choices=USER_ROLES, null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    def __str__(self):
        return "{}".format(self.username)


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=50)
    price = models.FloatField()
    published_data = models.DateField()

    def __str__(self):
        return str(self.title)


class Order(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    order_date = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.amount} - "{self.book}"s'